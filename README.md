# MyFlutterMigration

So why the effing hell should you the startup cofounder hire me as your
next Android Mobile Engineer?

Because, I am taking the CTO-CPO inspired approach to the move to 
FuchsiaOS-and-Flutter-Dart as the opportunity to align the full 
mobile team at your startup. You have already seen what a non-alignment
delivers in the Microsoft Not-aligned-approach to Mobile in that 
they lost time in in-fighting in facing the change from non-touch 
to touch mobile devices.

Your startup cannot afford such costly setbacks. So I took some months
doing a deep dive into issues to create some strategies.

## Flutter-Fuchsia Migration Strategies

### Legacy Way

Assumption is that you already have some tech debt in the form of an iphone app and 
a basic android app.

1. Align Android on Kotlin and Reactive mirroring same way reactive 
   is done using Dart in Flutter and use Anko rather than xml for layouts and use mvvm, 
   ie using kotlin reactive rather than rxjava extensions via coroutines.Align iOS app on Kotlin doing 
   reactive via coroutines and  use redux-fornm-of-flux.

2. At the same time start an internal port to the flutter Material widgets and dart on android and 
   flutter Cupertine Widgets and dart on iOS. Not just to train developers but to see 
   what plugins to flutter need to be built so that when Fuchsia devices hit the market one has 
   a battle plan of how to do the migration in full detail with everything already developed and 
   battle tested.

Costs, yes there will be some upfront costs as beyond 40-hour weeks for awhile. But, at the same time
due to starting early before 2024 your startup will have enough time if said extra dev time is 
spread evenly over those 5 years in definable small sprints.

Limitations: To get rid of the uses too much battery with items that require an often update of state 
              one has to use a State App arch such as the redux-form-of-flux and with android map 
              the redux map state to a less memory intensive form for parceables as parceables have 
             a binder 1meg transaction limit.


### Free Fall Way

The free fall way invovles doing the port upfront with the first android and iOS versions of the 
app in flutter and dart usinf redux-form-of-flux. 

Limitations:  The limiation is going to be how state is handled as the flutter framework 
              by itself with Google bloc patterm applied doesn't really address it whereas 
              if one removes bloc and uses the redux-form-of-flux app pattern it than is 
              fully addresses correctly. The only work is that on the android-and-fuchsia side
              one will have to do a redx-parceable helper as the redux map is generally 
              too big for parceable bundles(limit is 1meg as its a Binder transaction and that has a limit of 1meg).





## Summary

Basically, in my research of all issues I am DECLARING that Google is wrong in using BLOC app 
architecture patterns as on boht iOS and Android and later Fucshia its critical to handle the 
often updating of state differently to make usre its not being logically tied to widget updates as 
than it will impact the frame updates that each widget does and you want the state update not 
on a frame update.

Using Redux-form-of-flux with the right design choices and creating the right helpers will 
address the issue of making sure styate updating is not done on the frame redaws of widgets but 
in background so as not to slow down the frame draws and increasingly drain the battery on 
ios, android, and fuchsia.


Its contrarain, that is why you need me because the ones deep steeped in OOP-hr-style-experience 
are viewing things through some wrong assumptions and I AM NOT!

# Samples

    [DroidKt](https://gitlab.com/fred.grott/droidkt)

     My build devops setup for android kotlin app development meant to be used in migrating legacy code to at some point flutter.

    [DroidFlutterApp](https://gitlab.com/fred.grott/android_flutter_app)

      Android todo app using flutter and flux app arch with reactive-dart and fsms using the free falling approach to migrating in just starting over on flutter


     
     



# Contact


      fred     DOT

      grott    AT

        gmail   DOT


        com
